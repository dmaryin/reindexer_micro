package configs

import "github.com/spf13/viper"

type Config struct {
	ReindexerConfig ReindexerConfig `mapstructure:"reindexer"`
}

var GlobalConfig *Config

func LoadConfig(path string) (config Config, err error) {
	viper.AddConfigPath(path)
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")

	viper.AutomaticEnv()
	viper.SetDefault("reindexer.port", 6534)
	viper.SetDefault("reindexer.host", "localhost")
	viper.SetDefault("reindexer.user", "")
	viper.SetDefault("reindexer.password", "")
	viper.SetDefault("reindexer.database", "products")

	err = viper.ReadInConfig()
	if err != nil {
		return
	}

	err = viper.Unmarshal(&config)

	GlobalConfig = &config
	return
}
