package product

type Product struct {
	ID         int      `reindex:"id,,pk" json:"id"`
	Title      string   `json:"title"`
	Code       string   `json:"code"`
	Sort       int      `json:"-"`
	CategoryID int      `json:"categoryId"`
	Descr      string   `json:"description"`
	Options    []Option `json:"options"`
}

type Option struct {
	ID     int       `json:"id"`
	Title  string    `json:"title"`
	Values []*Values `json:"values,omitempty"`
	Sort   int       `json:"sort,omitempty"`
}

type Values struct {
	ID    int    `json:"id"`
	Sort  int    `json:"-"`
	Title string `json:"title"`
}
