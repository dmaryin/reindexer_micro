package product

import (
	"sync"

	rx "main/internal/pkg/reindexer"

	"github.com/restream/reindexer"
)

type Caches struct {
	Res map[int]*Product
	Mx  *sync.Mutex
}

var cachedResponse Caches = Caches{Res: make(map[int]*Product), Mx: new(sync.Mutex)}

func Cache(db *reindexer.Reindexer) (map[int]*Product, error) {
	if len(cachedResponse.Res) != 0 {
		return cachedResponse.Res, nil
	}

	cachedResponse.Mx.Lock()

	query := rx.
		GetConnectionReindexer(
			db,
			"products",
			Product{}).
		Query("products")

	iterator := query.Exec()
	defer iterator.Close()

	err := iterator.Error()

	for iterator.Next() {
		elem := iterator.Object().(*Product)
		cachedResponse.Res[elem.ID] = elem
	}

	cachedResponse.Mx.Unlock()

	return cachedResponse.Res, err
}
