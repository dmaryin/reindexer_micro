package product

import (
	"fmt"

	"github.com/restream/reindexer"

	rx "main/internal/pkg/reindexer"
)

func CreateProduct(products []Product) {
	db := rx.GetInstance()

	for _, product := range products {
		err := rx.
			GetConnectionReindexer(
				db,
				"products",
				Product{}).
			Upsert("products", &Product{
				ID:         product.ID,
				Title:      product.Title,
				Code:       product.Code,
				Sort:       product.Sort,
				CategoryID: product.CategoryID,
				Descr:      product.Descr,
				Options:    product.Options,
			})
		if err != nil {
			panic(err)
		}
	}
}

func UpdateProduct(id int, fieldName string, values interface{}) {
	db := rx.GetInstance()
	query := rx.
		GetConnectionReindexer(
			db,
			"products",
			Product{}).
		Query("products").
		Where("id", reindexer.EQ, id)

	switch values.(type) {
	case []Option:
		query = query.SetObject(fieldName, values)
	default:
		query = query.Set(fieldName, values)
	}

	iterator := query.Update()

	// Check the error
	if err := iterator.Error(); err != nil {
		fmt.Println("Update product err:", err)
	}
}

func GetProduct(id int) *Product {
	db := rx.GetInstance()
	cache, err := Cache(db)
	if err != nil {
		fmt.Println("Товар не найден")
		return nil
	}

	return cache[id]
}

func GetProducts() []Product {
	data := make([]Product, 0)
	limit := 10
	offset := 0
	count := true
	for count {
		db := rx.GetInstance()
		query := rx.
			GetConnectionReindexer(
				db,
				"products",
				Product{}).
			Query("products").
			Sort("options", false).
			Limit(limit).
			Offset(offset)

		iterator := query.Exec()
		if iterator.Count() == 0 {
			count = false
		}
		defer iterator.Close()

		for iterator.Next() {
			elem := iterator.Object().(*Product)
			data = append(data, *elem)
		}

		offset = offset + 1 + limit
		// Check the error
		if err := iterator.Error(); err != nil {
			count = false
			fmt.Println("Found products err:", err)
		}
	}

	return data
}
