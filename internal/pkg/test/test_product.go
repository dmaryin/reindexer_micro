package test

import (
	"fmt"

	p "main/internal/pkg/product"
)

func TestCreateProduct() {
	products := []p.Product{
		{
			ID:         1,
			Title:      "Кофта",
			Code:       "kofta",
			Sort:       1,
			CategoryID: 2,
			Descr:      "Кофта",
			Options: []p.Option{
				{
					ID:    1,
					Title: "Размер",
					Values: []*p.Values{
						{
							ID:    1,
							Title: "XS",
							Sort:  1,
						},
					},
					Sort: 1,
				},
			},
		},
		{
			ID:         2,
			Title:      "Юбка",
			Code:       "ubka",
			Sort:       2,
			CategoryID: 4,
			Descr:      "Юбка",
			Options: []p.Option{
				{
					ID:    1,
					Title: "Размер",
					Values: []*p.Values{
						{
							ID:    1,
							Title: "XS",
							Sort:  1,
						},
					},
					Sort: 1,
				},
			},
		},
	}

	p.CreateProduct(products)
}

func TestUpdateProduct() {
	id := 1
	fieldName := "options"
	values := []p.Option{
		{
			ID:    1,
			Title: "Размер",
			Values: []*p.Values{
				{
					ID:    1,
					Title: "S",
					Sort:  1,
				},
			},
			Sort: 1,
		},
	}

	p.UpdateProduct(id, fieldName, values)
}

func TestUpdateProductTwo() {
	id := 2
	fieldName := "description"
	values := "Юбка укороченная"

	p.UpdateProduct(id, fieldName, values)
}

func TestGetProduct() {
	id := 1
	product := p.GetProduct(id)
	fmt.Println("Found document:", product)
}

func TestGetProducts() {
	data := p.GetProducts()

	for _, value := range data {
		fmt.Println(value)
	}
}
