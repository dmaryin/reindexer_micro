package reindexer

import (
	"fmt"
	"main/configs"

	"github.com/restream/reindexer"
)

var instance *reindexer.Reindexer

type DB map[string]*reindexer.Reindexer

var dbMap = DB{}

func GetInstance() *reindexer.Reindexer {
	config := configs.GlobalConfig.ReindexerConfig

	if instance != nil {
		return instance
	}

	userLogin := fmt.Sprintf("%v:%v@", config.Username, config.Password)

	connectionPath := fmt.Sprintf("cproto://%v%v:%v/%v", userLogin, config.Host, config.Port, config.DBName)
	instance := reindexer.NewReindex(connectionPath, reindexer.WithCreateDBIfMissing())

	return instance
}

func GetConnectionReindexer(db *reindexer.Reindexer, namespace string, s interface{}) *reindexer.Reindexer {
	if dbMap[namespace] != nil {
		return dbMap[namespace]
	} else {
		db.OpenNamespace(namespace, reindexer.DefaultNamespaceOptions(), s)
		dbMap[namespace] = db
		return dbMap[namespace]
	}
}
