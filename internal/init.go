package internal

import (
	"fmt"
	"log"
	"main/configs"
	"time"

	"github.com/restream/reindexer"

	"main/internal/pkg/product"
	r "main/internal/pkg/reindexer"
	"main/internal/pkg/test"
)

var db *reindexer.Reindexer

func InitSubModules() {
	fmt.Println("Старт ...")

	SetConfigs()

	db = r.GetInstance()

	r.GetConnectionReindexer(db, "products", product.Product{})

	go func() {
		db := r.GetInstance()
		for {
			tick := time.NewTicker(time.Second * 60 * 15)
			<-tick.C

			product.Cache(db)
		}
	}()

	test.TestCreateProduct()
	test.TestGetProducts()
	test.TestUpdateProduct()
	test.TestGetProduct()
	test.TestUpdateProductTwo()
	test.TestGetProducts()

	fmt.Println("Тесты пройдены")
}

func SetConfigs() {
	_, err := configs.LoadConfig(".")

	if err != nil {
		log.Fatal("cannot load config:", err)
	}
}
