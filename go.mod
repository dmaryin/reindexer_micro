module main

go 1.16

require (
	github.com/ClickHouse/clickhouse-go/v2 v2.0.14
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/golang/snappy v0.0.4 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/iancoleman/orderedmap v0.2.0 // indirect
	github.com/restream/reindexer v3.2.1+incompatible
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/viper v1.8.1
	github.com/stretchr/testify v1.7.1
)
